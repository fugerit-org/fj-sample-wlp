package org.fugerit.java.sample.wlp.rest;

import java.io.File;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/server_conf/test")
public class ServerConfTest {

	@javax.ws.rs.GET
	@Path("/base_dir/{relPath}")
	public String getRandomNumber(@PathParam("relPath") String relPath) throws Exception {
		File file = new File( relPath );
		return file.getCanonicalPath();
	}

}
