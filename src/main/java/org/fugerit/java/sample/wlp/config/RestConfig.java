package org.fugerit.java.sample.wlp.config;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.fugerit.java.core.lang.helpers.ClassHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestConfig extends Application {

	private final static Logger logger = LoggerFactory.getLogger( RestConfig.class );
	
	private static void log( String message ) {
		System.out.println( "MESSAGE > "+message );
		logger.info( message );
	}
	
	
	private static void add( Set<Class<?>> set, String type ) {
		try {
			log( "RestConfig Adding type "+type );
			Object obj = (ClassHelper.newInstance( type ) );
			set.add( obj.getClass() );
		} catch ( Throwable e ) {
			logger.error( "Configuration error : "+e , e );
			e.printStackTrace();
		}
	}
	
	public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        /*
         *  ************ Available services services ************
         *  Sample test services
         */
        add( classes, "org.fugerit.java.sample.wlp.rest.RamdomTestRest" );
        add( classes, "org.fugerit.java.sample.wlp.rest.UploadTestRest" );
        /*
         * ************ Under development services ************
         * These services are still under development
         */
        //add( classes, "org.fugerit.java.sample.wlp.rest.DataSourceTestRest" );
        /*
         * ************ Config services ************
         * These services are provided only for test purpose, enable at your own risk : 
         */
        //add( classes, "org.fugerit.java.sample.wlp.rest.ServerConfTest" );
        return classes;
	}
		
}
