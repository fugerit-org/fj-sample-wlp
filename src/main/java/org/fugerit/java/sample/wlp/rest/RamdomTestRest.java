package org.fugerit.java.sample.wlp.rest;

import java.security.SecureRandom;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/random_test")
public class RamdomTestRest {

	private final static Logger logger = LoggerFactory.getLogger( RamdomTestRest.class );
	
	@javax.ws.rs.GET
	@Path("/max/{max}")
	public String getRandomNumber(@PathParam("max") String max) throws Exception {
		Long m = Long.parseLong( max );
		String result = "Generated number : "+String.valueOf( (long)((SecureRandom.getInstanceStrong().nextDouble()*m)) );
		logger.info( "Request number up to max : '{}' -> result : '{}'", max, result );
		return result;
	}

}
