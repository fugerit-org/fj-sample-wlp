package org.fugerit.java.sample.wlp.rest;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@Path("/upload")
public class UploadTestRest {

	private final static Logger logger = LoggerFactory.getLogger( UploadTestRest.class );

	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response upload(
			@FormDataParam("fileName") String fileName,
			@FormDataParam("file") byte[] file
			) throws Exception{
		Response res = null;
		logger.info( "fileName -> {}", fileName );
		logger.info( "file size -> {} "+file.length );
		Response.ok( fileName ).build();
		return res;
	}

}
