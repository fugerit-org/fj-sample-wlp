package org.fugerit.java.sample.wlp.rest;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.ws.rs.Path;

@Path("/ds_test/metadata")
public class DataSourceTestRest {

	@javax.ws.rs.GET
	public String metadata() throws Exception {
		Context initCtx = new InitialContext();
        Context envCtx = (Context) initCtx.lookup("java:comp/env");
        DataSource ds = (DataSource) envCtx.lookup( "jdbc/data-source" );
        StringBuilder buffer = new StringBuilder();
        try (Connection conn = ds.getConnection()) {
        } catch (Exception e) {
        	buffer.append( "Error : " );
        	buffer.append( e.toString() );
        	e.printStackTrace();
        }
		return buffer.toString();
	}

}
